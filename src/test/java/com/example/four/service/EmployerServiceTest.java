package com.example.four.service;

import com.example.four.Dao.EmployerJpaRepository;
import com.example.four.model.Employer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class EmployerServiceTest {
    @Mock
    private EmployerJpaRepository employerJpaRepository;

    @InjectMocks
    private EmployerService employerService;

    @Captor
    private ArgumentCaptor<Employer> employerArgumentCaptor;

    @Test
    public void testGetEmployerFullInfo(){
        Employer employerExpected = new Employer();
        when(employerJpaRepository.getOne(1L))
                .thenReturn(employerExpected);
        Employer employerActual = employerService.getEmployerFullInfo(1L);
        assertEquals(employerExpected, employerActual);
    }

    @Test
    public void testGetAllEmployers(){
        Employer employerExpected = new Employer();
        when(employerJpaRepository.findAll())
                .thenReturn(List.of(employerExpected));
        List<Employer> employersActual = employerService.getAllEmployers();
        assertFalse(employersActual.isEmpty());
        assertEquals(1, employersActual.size());
        assertEquals(employerExpected, employersActual.get(0));
    }

    @Test
    public void testCreateEmployer(){
        Employer employerExpected = new Employer();
        when(employerJpaRepository.save(employerExpected))
                .thenReturn(employerExpected);
        Employer employerActual = employerService.createEmployer(employerExpected);
        assertEquals(employerExpected, employerActual);

        verify(employerJpaRepository).save(employerArgumentCaptor.capture());
        Employer employeeActualArgument = employerArgumentCaptor.getValue();
        assertEquals(employerExpected, employeeActualArgument);
    }
    @Test
    public void testUpdateEmployer(){
        Employer employerExpected = new Employer();
        employerExpected.setId(1L);
        when(employerJpaRepository.save(employerExpected))
                .thenReturn(employerExpected);
        Employer employerActual = employerService.updateEmployer(employerExpected);
        assertEquals(employerExpected, employerActual);

        verify(employerJpaRepository).save(employerArgumentCaptor.capture());
        Employer employeeActualArgument = employerArgumentCaptor.getValue();
        assertEquals(employerExpected, employeeActualArgument);
    }
    @Test
    public void testDeleteEmployer(){
        Employer employerExpected = new Employer();
        employerService.deleteEmployer(employerExpected);

        verify(employerJpaRepository).delete(employerArgumentCaptor.capture());
        Employer employeeActualArgument = employerArgumentCaptor.getValue();
        assertEquals(employerExpected, employeeActualArgument);
    }
    @Test
    public void testDeleteEmployerById(){
        Employer employerExpected = new Employer();
        employerExpected.setId(1L);
        when(employerJpaRepository.getOne(1L))
                .thenReturn(employerExpected);
        employerService.deleteEmployerById(1L);

        verify(employerJpaRepository).delete(employerArgumentCaptor.capture());
        Employer employeeActualArgument = employerArgumentCaptor.getValue();
        assertEquals(employerExpected, employeeActualArgument);
    }
}
