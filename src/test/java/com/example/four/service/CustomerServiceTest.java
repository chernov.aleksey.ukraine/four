package com.example.four.service;

import com.example.four.Dao.AccountJpaRepository;
import com.example.four.Dao.CustomerJpaRepository;
import com.example.four.model.Account;
import com.example.four.model.Customer;
import com.example.four.utilities.Currency;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {
    @Mock
    private CustomerJpaRepository customerJpaRepository;
    @Mock
    private AccountJpaRepository accountJpaRepository;
    @InjectMocks
    private CustomerService customerService;
    @Captor
    private ArgumentCaptor<Customer> customerArgumentCaptor;
    @Captor
    private ArgumentCaptor<Account> accountArgumentCaptor;
    @Captor
    private ArgumentCaptor<Long> accountIdArgumentCaptor;

    @Test
    public void testGetCustomerFullInfo(){
        Customer customerExpected = new Customer();
        when(customerJpaRepository.findById(1L))
                .thenReturn(Optional.of(customerExpected));
        Customer customerActual = customerService.getCustomerFullInfo(1L);
        assertEquals(customerExpected, customerActual);
    }

    @Test
    public void testGetAllCustomers(){
        Customer customerExpected = new Customer();
        when(customerJpaRepository.findAll(any(Pageable.class)))
                .thenReturn(new PageImpl<>(List.of(customerExpected)));
        List<Customer> pageCustomerActualWithParameters = customerService.getAllCustomers(0,1);
        assertFalse(pageCustomerActualWithParameters.isEmpty());
        assertEquals(1, pageCustomerActualWithParameters.size());
        assertEquals(customerExpected, pageCustomerActualWithParameters.get(0));

        List<Customer> pageCustomerActualWithoutParameters = customerService.getAllCustomers();
        assertFalse(pageCustomerActualWithoutParameters.isEmpty());
        assertEquals(1, pageCustomerActualWithoutParameters.size());
        assertEquals(customerExpected, pageCustomerActualWithoutParameters.get(0));
    }

    @Test
    public void testCreateCustomer(){
        Customer customerExpected = new Customer();
        when(customerJpaRepository.save(customerExpected))
                .thenReturn(customerExpected);
        Customer customerActual = customerService.createCustomer(customerExpected);
        assertEquals(customerExpected, customerActual);

        verify(customerJpaRepository).save(customerArgumentCaptor.capture());
        Customer customerActualArgument = customerArgumentCaptor.getValue();
        assertEquals(customerExpected, customerActualArgument);
    }
    @Test
    public void testUpdateCustomer(){
        Customer customerExpected = new Customer();
        customerExpected.setId(1L);
        when(customerJpaRepository.save(customerExpected))
                .thenReturn(customerExpected);
        Customer customerActual = customerService.updateCustomer(customerExpected);
        assertEquals(customerExpected, customerActual);

        verify(customerJpaRepository).save(customerArgumentCaptor.capture());
        Customer customerActualArgument = customerArgumentCaptor.getValue();
        assertEquals(customerExpected, customerActualArgument);
    }
    @Test
    public void testDeleteCustomer(){
        Customer customerExpected = new Customer();
        customerService.deleteCustomer(customerExpected);

        verify(customerJpaRepository).delete(customerArgumentCaptor.capture());
        Customer customerActualArgument = customerArgumentCaptor.getValue();
        assertEquals(customerExpected, customerActualArgument);
    }
    @Test
    public void testDeleteCustomerById(){
        Customer customerExpected = new Customer();
        when(customerJpaRepository.findById(1L))
                .thenReturn(Optional.of(customerExpected));
        customerService.deleteCustomerById(1L);

        verify(customerJpaRepository).delete(customerArgumentCaptor.capture());
        Customer customerActualArgument = customerArgumentCaptor.getValue();
        assertEquals(customerExpected, customerActualArgument);
    }

    @Test
    public void testOpenAccount(){
        Customer customerExpected = new Customer();
        customerExpected.setId(1L);
        when(customerJpaRepository.findById(1L))
                .thenReturn(Optional.of(customerExpected));
        Customer customerActual = customerService.openAccount(customerExpected, Currency.UAH);

        verify(accountJpaRepository).save(accountArgumentCaptor.capture());
        Account accountActualArgument = accountArgumentCaptor.getValue();
        assertEquals(customerExpected, accountActualArgument.getCustomer());
        assertEquals(customerExpected, customerActual);
    }

    @Test
    public void testDeleteAccount(){
        Customer customerExpected = new Customer();
        customerExpected.setId(1L);
        Account account = new Account("1111111", Currency.UAH, 0D, customerExpected);
        account.setId(1L);
        when(customerJpaRepository.findById(1L))
                .thenReturn(Optional.of(customerExpected));
        when(accountJpaRepository.findByNumber("1111111"))
                .thenReturn(account);
        Customer customerActual = customerService.deleteAccount(customerExpected, "1111111");

        verify(accountJpaRepository).deleteById(accountIdArgumentCaptor.capture());
        Long accountIdActualArgument = accountIdArgumentCaptor.getValue();
        assertEquals(account.getId(), accountIdActualArgument);
        assertEquals(customerExpected, customerActual);
    }
}
