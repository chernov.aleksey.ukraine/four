package com.example.four.service;

import com.example.four.Dao.AccountJpaRepository;
import com.example.four.model.Account;
import com.example.four.model.Customer;
import com.example.four.utilities.Currency;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {
    @Mock
    private AccountJpaRepository accountJpaRepository;
    @InjectMocks
    private AccountService accountService;
    @Captor
    private ArgumentCaptor<Account> accountArgumentCaptor;

    @Test
    public void testDepositAccount(){
        Customer customerExpected = new Customer();
        Account account = new Account("1111111", Currency.UAH, 0D, customerExpected);
        account.setId(1L);
        when(accountJpaRepository.findByNumber("1111111"))
                .thenReturn(account);
        Boolean AccountDepositResult = accountService.depositAccount("1111111", 50.20);

        verify(accountJpaRepository).save(accountArgumentCaptor.capture());
        Account accountActualArgument = accountArgumentCaptor.getValue();
        assertEquals(account, accountActualArgument);
        assertEquals(true, AccountDepositResult);
        assertEquals(50.20, account.getBalance());
    }
    @Test
    public void testWithdrawalMoney(){
        Customer customerExpected = new Customer();
        Account account = new Account("1111111", Currency.UAH, 50.50, customerExpected);
        account.setId(1L);
        when(accountJpaRepository.findByNumber("1111111"))
                .thenReturn(account);
        Boolean AccountDepositResult = accountService.withdrawalMoney("1111111", 10.50);

        verify(accountJpaRepository).save(accountArgumentCaptor.capture());
        Account accountActualArgument = accountArgumentCaptor.getValue();
        assertEquals(account, accountActualArgument);
        assertEquals(true, AccountDepositResult);
        assertEquals(40.00, account.getBalance());
    }
    @Test
    public void testSendMoney(){
        Customer customerExpected = new Customer();
        Account accountSender = new Account("1111111", Currency.UAH, 50.50, customerExpected);
        accountSender.setId(1L);
        Account accountReceiver = new Account("2222222", Currency.UAH, 0D, customerExpected);
        accountReceiver.setId(2L);
        when(accountJpaRepository.findByNumber("1111111"))
                .thenReturn(accountSender);
        when(accountJpaRepository.findByNumber("2222222"))
                .thenReturn(accountReceiver);
        Boolean AccountDepositResult = accountService.sendMoney("1111111", "2222222",50.50);

        verify(accountJpaRepository, times(2)).save(accountArgumentCaptor.capture());
        List<Account> accountActualArgument = accountArgumentCaptor.getAllValues();
        assertTrue(accountActualArgument.containsAll(List.of(accountSender, accountReceiver)));
        assertEquals(true, AccountDepositResult);
        assertEquals(0D, accountSender.getBalance());
        assertEquals(50.50, accountReceiver.getBalance());
    }
}
