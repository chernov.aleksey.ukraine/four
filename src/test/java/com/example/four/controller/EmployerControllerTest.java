package com.example.four.controller;

import com.example.four.filter.JwtFilter;
import com.example.four.model.Customer;
import com.example.four.model.DTOMapper.EmployerDtoMapperRequest;
import com.example.four.model.DTOMapper.EmployerDtoMapperResponse;
import com.example.four.model.Employer;
import com.example.four.model.SysRole;
import com.example.four.model.SysUser;
import com.example.four.service.EmployerService;
import com.example.four.service.JwtProvider;
import com.example.four.service.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.NonNull;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EmployerController.class)
public class EmployerControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private EmployerService employerService;
    @Autowired
    private EmployerDtoMapperRequest employerDtoMapperRequest;
    @Autowired
    private EmployerDtoMapperResponse employerDtoMapperResponse;

    @Autowired
    private JwtFilter jwtFilter;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @MockBean
    private UserService userService;

    @TestConfiguration
    static class TestConfig{
        @Bean
        public EmployerDtoMapperResponse employerDtoMapperResponse(){
            return new EmployerDtoMapperResponse();
        }
        @Bean
        public EmployerDtoMapperRequest employerDtoMapperRequest(){
            return new EmployerDtoMapperRequest();
        }
        @Bean
        public JwtFilter jwtFilter(){
            return new JwtFilter(new JwtProvider("LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2V",
                    "LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2V"));
        };

    }

    public String generateAccessToken(@NonNull SysUser user) {
        final LocalDateTime now = LocalDateTime.now();
        final Instant accessExpirationInstant = now.plusMinutes(5).atZone(ZoneId.systemDefault()).toInstant();
        final Date accessExpiration = Date.from(accessExpirationInstant);
        return Jwts.builder()
                .setSubject(user.getUserName())
                .setExpiration(accessExpiration)
                .signWith(Keys.hmacShaKeyFor(Decoders.BASE64.decode("LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2V")))
                .claim("roles", user.getRoles())
                .claim("firstName", user.getUserName())
                .compact();
    }
    @Test
    public void getAllEmployersTest() throws Exception {
        SysUser sysUser = new SysUser(1L, "TestUser", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(1L, "USER", new SysUser()))));
        Employer employerExpected = new Employer(1L, "TestName", "TestAddress");
        when(employerService.getAllEmployers())
                .thenReturn(List.of(employerExpected));
        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        this.mockMvc.perform(MockMvcRequestBuilders.get("/employers/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + this.generateAccessToken(sysUser)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.is("TestName")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].address", Matchers.is("TestAddress")));
    }
    @Test
    public void getEmployerFullInfoTest() throws Exception {
        SysUser sysUser = new SysUser(1L, "TestUser", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(1L, "USER", new SysUser()))));
        Employer employerExpected = new Employer(1L, "TestName", "TestAddress");
        when(employerService.getEmployerFullInfo(1L))
                .thenReturn(employerExpected);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/employers/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + this.generateAccessToken(sysUser)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("TestName")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address", Matchers.is("TestAddress")));
    }
    @Test
    public void postNewEmployerTest() throws Exception {
        SysUser sysUser = new SysUser(1L, "TestUser", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(1L, "USER", new SysUser()))));
        Employer employerExpected = new Employer(1L, "TestName", "TestAddress");
        when(employerService.createEmployer(employerExpected))
                .thenReturn(employerExpected);
        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        this.mockMvc.perform(MockMvcRequestBuilders.post("/employers/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + this.generateAccessToken(sysUser))
                        .content(
                                """
                                 {
                                    "name": "TestName",
                                    "address": "TestAddress"
                                }
                                """
                        ))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("TestName")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address", Matchers.is("TestAddress")));
    }
    @Test
    public void updateEmployerTest() throws Exception {
        SysUser sysUser = new SysUser(1L, "TestUser", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(1L, "USER", new SysUser()))));
        Employer employerExpected = new Employer(1L, "TestName", "TestAddress");
        when(employerService.updateEmployer(employerExpected))
                .thenReturn(employerExpected);
        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        this.mockMvc.perform(MockMvcRequestBuilders.put("/employers/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + this.generateAccessToken(sysUser))
                        .content(
                                """
                                 {
                                    "id": "1",
                                    "name": "TestName",
                                    "address": "TestAddress"
                                }
                                """
                        ))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("TestName")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address", Matchers.is("TestAddress")));
    }
    @Test
    public void deleteEmployerTest() throws Exception {
        SysUser sysUser = new SysUser(1L, "TestUser", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(1L, "USER", new SysUser()))));
        Employer employerExpected = new Employer(1L, "TestName", "TestAddress");
        when(employerService.deleteEmployerById(1L))
                .thenReturn(true);
        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        this.mockMvc.perform(MockMvcRequestBuilders.delete("/employers/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + this.generateAccessToken(sysUser)))
                .andExpect(status().isOk());
    }
}
