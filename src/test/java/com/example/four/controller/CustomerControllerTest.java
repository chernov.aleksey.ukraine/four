package com.example.four.controller;

import com.example.four.filter.JwtFilter;
import com.example.four.model.Customer;
import com.example.four.model.DTOMapper.CustomerDtoMapperRequest;
import com.example.four.model.DTOMapper.CustomerDtoMapperResponse;
import com.example.four.model.SysRole;
import com.example.four.model.SysUser;
import com.example.four.service.CustomerService;
import com.example.four.service.JwtProvider;
import com.example.four.service.UserService;
import com.example.four.utilities.Currency;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.NonNull;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CustomerController.class)
public class CustomerControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CustomerService customerService;
    @Autowired
    private CustomerDtoMapperResponse customerDtoMapperResponse;
    @Autowired
    private CustomerDtoMapperRequest customerDtoMapperRequest;
    @Autowired
    private JwtFilter jwtFilter;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @MockBean
    private UserService userService;


    @TestConfiguration
    static class TestConfig{
        @Bean
        public CustomerDtoMapperResponse customerDtoMapperResponse(){
            return new CustomerDtoMapperResponse();
        }
        @Bean
        public CustomerDtoMapperRequest customerDtoMapperRequest(){
            return new CustomerDtoMapperRequest();
        }

        @Bean
        public JwtFilter jwtFilter(){
            return new JwtFilter(new JwtProvider("LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2V",
                                                    "LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2V"));
        };

    }

    public String generateAccessToken(@NonNull SysUser user) {
        final LocalDateTime now = LocalDateTime.now();
        final Instant accessExpirationInstant = now.plusMinutes(5).atZone(ZoneId.systemDefault()).toInstant();
        final Date accessExpiration = Date.from(accessExpirationInstant);
        return Jwts.builder()
                .setSubject(user.getUserName())
                .setExpiration(accessExpiration)
                .signWith(Keys.hmacShaKeyFor(Decoders.BASE64.decode("LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2V")))
                .claim("roles", user.getRoles())
                .claim("firstName", user.getUserName())
                .compact();
    }
    @Test
    @WithMockUser(value = "spring")
    public void getAllUsersTest() throws Exception {
        SysUser sysUser = new SysUser(1L, "TestUser", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(1L, "USER", new SysUser()))));
        Customer customerExpected = new Customer(1L, "Ivan Petrenko", "ivanpetrenko@mail.com", 25);
        when(customerService.getAllCustomers())
                .thenReturn(List.of(customerExpected));
        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        this.mockMvc.perform(MockMvcRequestBuilders.get("/customers/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + this.generateAccessToken(sysUser)))//
                 //.header("authorization", "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhIiwiZXhwIjoxNjg1NDUyODAzLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlTmFtZSI6IlVTRVIifV0sImZpcnN0TmFtZSI6ImEifQ.3skfWP6_6bdXrW8ZQ64KYzl4F0WwSAmlh_YZCY3kwPIjXqzEHwCoD7p9-bZOEPiXKYepDSJVNRI2jKr8O6b8nw"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.is("Ivan Petrenko")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age", Matchers.is(25)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].email", Matchers.is("ivanpetrenko@mail.com")));
    }
    @Test
    public void getAllUsersPagebleTest() throws Exception {
        SysUser sysUser = new SysUser(1L, "TestUser", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(1L, "USER", new SysUser()))));
        Customer customerExpected = new Customer(1L, "Ivan Petrenko", "ivanpetrenko@mail.com", 25);
        when(customerService.getAllCustomers(0, 1))
                .thenReturn(List.of(customerExpected));
        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        this.mockMvc.perform(MockMvcRequestBuilders.get("/customers/0/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + this.generateAccessToken(sysUser)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.is("Ivan Petrenko")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age", Matchers.is(25)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].email", Matchers.is("ivanpetrenko@mail.com")));
    }
    @Test
    public void getUserFullInfoTest() throws Exception {
        SysUser sysUser = new SysUser(1L, "TestUser", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(1L, "USER", new SysUser()))));
        Customer customerExpected = new Customer(1L, "Ivan Petrenko", "ivanpetrenko@mail.com", 25);
        when(customerService.getCustomerFullInfo(1L))
                .thenReturn(customerExpected);
        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        this.mockMvc.perform(MockMvcRequestBuilders.get("/customers/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + this.generateAccessToken(sysUser)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("Ivan Petrenko")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age", Matchers.is(25)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email", Matchers.is("ivanpetrenko@mail.com")));
    }
    @Test
    public void postNewUserTest() throws Exception {
        SysUser sysUser = new SysUser(1L, "TestUser", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(1L, "USER", new SysUser()))));
        Customer customerExpected = new Customer("Ivan Petrenko", "ivanpetrenko@mail.com", 25);
        when(customerService.createCustomer(customerExpected))
                .thenReturn(customerExpected);
        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        this.mockMvc.perform(MockMvcRequestBuilders.post("/customers/")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + this.generateAccessToken(sysUser))
                .content(
                        """
                         {
                            "email": "ivanpetrenko@mail.com",
                            "phoneNumber": "+38067123456789",
                            "name": "Ivan Petrenko",
                            "age": 25,
                            "password": "ivanPetrenko"
                        }
                        """
                ))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("Ivan Petrenko")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age", Matchers.is(25)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email", Matchers.is("ivanpetrenko@mail.com")));
    }
    @Test
    public void postUpdateUserTest() throws Exception {
        SysUser sysUser = new SysUser(1L, "TestUser", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(1L, "USER", new SysUser()))));
        Customer customerExpected = new Customer(1L, "Ivan Petrenko", "ivanpetrenko@mail.com", 25);
        when(customerService.updateCustomer(customerExpected))
                .thenReturn(customerExpected);
        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        this.mockMvc.perform(MockMvcRequestBuilders.put("/customers/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + this.generateAccessToken(sysUser))
                        .content(
                                """
                                 {
                                    "email": "ivanpetrenko@mail.com",
                                    "phoneNumber": "+38067123456789",
                                    "name": "Ivan Petrenko",
                                    "age": 25,
                                    "password": "ivanPetrenko"
                                }
                                """
                        ))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("Ivan Petrenko")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age", Matchers.is(25)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email", Matchers.is("ivanpetrenko@mail.com")));
    }
    @Test
    public void deleteCustomerTest() throws Exception {
        SysUser sysUser = new SysUser(1L, "TestUser", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(1L, "USER", new SysUser()))));
        Customer customerExpected = new Customer(1L, "Ivan Petrenko", "ivanpetrenko@mail.com", 25);
        when(customerService.deleteCustomerById(1L))
                .thenReturn(true);
        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        this.mockMvc.perform(MockMvcRequestBuilders.delete("/customers/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + this.generateAccessToken(sysUser)))
                .andExpect(status().isOk());
    }
    @Test
    public void openAccountTest() throws Exception {
        SysUser sysUser = new SysUser(1L, "TestUser", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(1L, "USER", new SysUser()))));
        Customer customerExpected = new Customer(1L, "Ivan Petrenko", "ivanpetrenko@mail.com", 25);
        when(customerService.getCustomerFullInfo(1L))
                .thenReturn(customerExpected);
        when(customerService.openAccount(customerExpected, Currency.UAH))
                .thenReturn(customerExpected);
        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        this.mockMvc.perform(MockMvcRequestBuilders.post("/customers/1/account")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + this.generateAccessToken(sysUser))
                        .content(
                                """
                                 {
                                    "currency": "UAH"
                                }
                                """
                        ))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("Ivan Petrenko")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age", Matchers.is(25)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email", Matchers.is("ivanpetrenko@mail.com")));

    }
    @Test
    public void deleteAccountTest() throws Exception {
        SysUser sysUser = new SysUser(1L, "TestUser", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(1L, "USER", new SysUser()))));
        Customer customerExpected = new Customer(1L, "Ivan Petrenko", "ivanpetrenko@mail.com", 25);
        when(customerService.getCustomerFullInfo(1L))
                .thenReturn(customerExpected);
        when(customerService.deleteAccount(customerExpected, "11111"))
                .thenReturn(customerExpected);
        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        this.mockMvc.perform(MockMvcRequestBuilders.delete("/customers/1/account")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + this.generateAccessToken(sysUser))
                        .content(
                                """
                                 {
                                    "accountNumber": "11111"
                                }
                                """
                        ))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("Ivan Petrenko")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age", Matchers.is(25)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email", Matchers.is("ivanpetrenko@mail.com")));
    }
}
