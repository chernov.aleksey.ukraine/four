
package com.example.four.controller;

import com.example.four.filter.JwtFilter;
import com.example.four.model.Customer;
import com.example.four.service.AccountService;
import com.example.four.service.JwtProvider;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AccountController.class)
public class AccountControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private AccountService accountService;
    @MockBean
    private JwtFilter jwtFilter;
    @MockBean
    private  JwtProvider jwtProvider;

    @BeforeEach
    public void setUp() {
        when(jwtProvider.validateAccessToken(any(String.class))).thenReturn(true);
    }
    @Test
    public void depositMoneyTest() throws Exception {
        when(accountService.depositAccount("11111", 50D))
                .thenReturn(true);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/accounts/deposit")
                        .contentType("application/json")
                        .content(
                                """
                                 {
                                    "accountNumber": "11111",
                                    "amount": "50.00"
                                }
                                """
                        ))
                .andExpect(status().isOk());
    }
    @Test
    public void withdrawalMoneyTest() throws Exception {
        when(accountService.withdrawalMoney("11111", 50D))
                .thenReturn(true);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/accounts/withdrawal")
                        .contentType("application/json")
                        .content(
                                """
                                 {
                                    "accountNumber": "11111",
                                    "amount": "50.00"
                                }
                                """
                        ))
                .andExpect(status().isOk());
    }

    @Test
    public void sendMoneyTest() throws Exception {
        when(accountService.sendMoney("11111", "22222", 50D))
                .thenReturn(true);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/accounts/send")
                        .contentType("application/json")
                        .content(
                                """
                                 {
                                    "accountNumberSender": "11111",
                                    "accountNumberReceiver": "22222",
                                    "amount": "50.00"
                                }
                                """
                        ))
                .andExpect(status().isOk());
    }
}

