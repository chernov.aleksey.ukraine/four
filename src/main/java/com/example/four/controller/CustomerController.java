package com.example.four.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.example.four.model.DTO.CustomerDtoRequest;
import com.example.four.model.DTO.CustomerDtoResponse;
import com.example.four.model.DTOMapper.CustomerDtoMapperRequest;
import com.example.four.model.DTOMapper.CustomerDtoMapperResponse;
import com.example.four.utilities.Currency;
import com.example.four.service.CustomerService;
import com.example.four.model.Customer;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = {"http://localhost:3000"})
@RequestMapping("/customers")
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;
    private final CustomerDtoMapperResponse customerDtoMapperResponse;
    private final CustomerDtoMapperRequest customerDtoMapperRequest;

    @GetMapping("/")
    public ResponseEntity<?> getAllUsers() {
        return ResponseEntity.ok(customerService.getAllCustomers().stream()
                .map(customerDtoMapperResponse::convertToDto)
                .collect(Collectors.toList()));
    }
    @GetMapping("/{page}/{size}")
    public ResponseEntity<?> getAllUsers(@PathVariable Integer page, @PathVariable Integer size) {
        return ResponseEntity.ok(customerService.getAllCustomers(page, size).stream()
                .map(customerDtoMapperResponse::convertToDto)
                .collect(Collectors.toList()));
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getUserFullInfo(@PathVariable Long id){
        return ResponseEntity.ok(customerDtoMapperResponse.convertToDto(customerService.getCustomerFullInfo(id)));
    }
    @PostMapping("/")
    public CustomerDtoResponse postNewUser(@RequestBody CustomerDtoRequest customerDtoRequest){
        Customer customer = customerDtoMapperRequest.convertToEntity(customerDtoRequest);
        return customerDtoMapperResponse.convertToDto(customerService.createCustomer(customer));
    }
    @PutMapping("/")
    public CustomerDtoResponse updateUser(@RequestBody CustomerDtoRequest customerDtoRequest){
        Customer customer = customerDtoMapperRequest.convertToEntity(customerDtoRequest);
        return customerDtoMapperResponse.convertToDto(customerService.updateCustomer(customer));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable Long id){
        return customerService.deleteCustomerById(id) ? ResponseEntity.ok("Success") : ResponseEntity.badRequest().body("Customer not found");
    }
    @PostMapping("/{id}/account")
    public ResponseEntity<?> openAccount(@PathVariable Long id, @RequestBody String currency) throws JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode newAccountCurrency = objectMapper.readTree(currency);
        String curren = newAccountCurrency.get("currency").asText();
        Currency curr = Currency.forValue(curren);

         Customer customer = customerService.getCustomerFullInfo(id);
         if(customer == null) {
             ResponseEntity.badRequest().body("Customer not found");
         }
        return ResponseEntity.ok(customerDtoMapperResponse.convertToDto(customerService.openAccount(customer, curr)));
    }
    @DeleteMapping("/{id}/account")
    public ResponseEntity<?> deleteAccount(@PathVariable Long id, @RequestBody String accountNumber ) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode nameNode = mapper.readTree(accountNumber);
        Customer customer = customerService.getCustomerFullInfo(id);
        if(customer == null) {
            ResponseEntity.badRequest().body("Customer not found");
        }
        return ResponseEntity.ok(customerDtoMapperResponse.convertToDto(customerService.deleteAccount(customer, nameNode.get("accountNumber").asText())));
    }
}
