package com.example.four.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.example.four.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = {"http://localhost:3000"})
@RequestMapping("/accounts")
public class AccountController {
    @Autowired
    private AccountService accountService;


    @PostMapping("/deposit")
    public ResponseEntity<?> depositMoney(@RequestBody String parametersJson) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode nameNodeAccountNumber = mapper.readTree(parametersJson);
        String accountNumber = nameNodeAccountNumber.get("accountNumber").asText();
        Double amount = Double.parseDouble(nameNodeAccountNumber.get("amount").asText());

        return accountService.depositAccount(accountNumber, amount)
                ? ResponseEntity.ok("Success")
                : ResponseEntity.badRequest().body("Depositing of the account is failed");
    }

    @PostMapping("/withdrawal")
    public ResponseEntity<?> withdrawalMoney(@RequestBody String parametersJson) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode nameNodeAccountNumber = mapper.readTree(parametersJson);
        String accountNumber = nameNodeAccountNumber.get("accountNumber").asText();
        Double amount = Double.parseDouble(nameNodeAccountNumber.get("amount").asText());
        return accountService.withdrawalMoney(accountNumber, amount)
                ? ResponseEntity.ok("Success")
                : ResponseEntity.badRequest().body("Withdrawing from the account is failed");
    }

    @PostMapping("/send")
    public ResponseEntity<?> sendMoney(@RequestBody String parametersJson) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode nameNodeAccountNumber = mapper.readTree(parametersJson);
        String accountNumberSender = nameNodeAccountNumber.get("accountNumberSender").asText();
        String accountNumberReceiver = nameNodeAccountNumber.get("accountNumberReceiver").asText();
        Double amount = Double.parseDouble(nameNodeAccountNumber.get("amount").asText());
        return accountService.sendMoney(accountNumberSender, accountNumberReceiver, amount)
                ? ResponseEntity.ok("Success")
                : ResponseEntity.badRequest().body("Sending money is failed");
    }
}
