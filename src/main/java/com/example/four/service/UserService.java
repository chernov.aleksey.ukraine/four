package com.example.four.service;

import com.example.four.Dao.UserJpaRepository;
import com.example.four.model.SysUser;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserJpaRepository userJpaRepository;

    public Optional<SysUser> getByLogin(@NonNull String login) {

        return userJpaRepository.findUsersByUserName(login);
    }

}