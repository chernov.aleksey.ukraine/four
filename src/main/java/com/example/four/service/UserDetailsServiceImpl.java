package com.example.four.service;

import com.example.four.Dao.UserJpaRepository;
import com.example.four.model.SysRole;
import com.example.four.model.SysUser;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserJpaRepository userJpaRepository;
    private final PasswordEncoder passwordEncoder;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<SysUser> sysUser = userJpaRepository.findUsersByUserName(username);
        if (sysUser.isEmpty()) {
            throw new UsernameNotFoundException(username);
        }
        List<SimpleGrantedAuthority> authorities = sysUser.get().getRoles().stream()
                .map(r -> new SimpleGrantedAuthority(r.getRoleName()))
                .toList();
        return new User(sysUser.get().getUserName(), sysUser.get().getEncryptedPassword(), authorities);
//        return User.withUsername("a").password(passwordEncoder.encode("a")).authorities("USER").build();
    }

    public void createNew(String userName, String password) {
        SysRole role = new SysRole(null, "USER", null);
        SysUser user = new SysUser(null,
                userName,
                passwordEncoder.encode(password),
                true,
                Set.of(role));
        role.setUser(user);
        userJpaRepository.save(user);
    }

    public List<SysUser> findAll() {
        return userJpaRepository.findAll();
    }

//    public void createNew() {
//        SysRole role = new SysRole(null, "USER", null);
//        SysUser user = new SysUser(1L,
//                "new",
//                passwordEncoder.encode("new"),
//                true,
//                Set.of(role));
//        role.setSysUser(user);
//        userRepository.save(user);
//    }


}
