package com.example.four.service;

import com.example.four.Dao.UserJpaRepository;
import com.example.four.model.SysUser;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuditorAwareImpl implements AuditorAware<SysUser> {

    private final UserJpaRepository userJpaRepository;
    @Override
    public Optional<SysUser> getCurrentAuditor() {
        // return Optional.of(new SysUser(20L, "Aware", "enc", true, null));

        return Optional.of(userJpaRepository.findUsersByUserName(SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal()
                .toString())
                .get());
    }
}
