package com.example.four.model.DTO;

import com.example.four.model.Customer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmployerDtoResponse {

    private Long id;
    private String name;
    private String address;
    private Set<Customer> customers;
}
