package com.example.four.model.DTO;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmployerDtoRequest {
    @NotNull
    private Long id;
    @NotNull
    @Size(min = 3, message = "employer name should have at least 3 characters")
    private String name;
    @NotNull
    @Size(min = 3, message = "employer address should have at least 3 characters")
    private String address;
}
