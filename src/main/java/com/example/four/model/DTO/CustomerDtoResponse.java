package com.example.four.model.DTO;

import com.example.four.model.Account;
import com.example.four.model.Employer;
import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDtoResponse {

    private Long id;
    private String name;
    private String email;
    private Integer age;
    private String phone;
    private List<Account> accounts;
    private Set<Employer> employers;
}
