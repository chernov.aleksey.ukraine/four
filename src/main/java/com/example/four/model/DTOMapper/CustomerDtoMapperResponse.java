package com.example.four.model.DTOMapper;

import com.example.four.model.Customer;
import com.example.four.model.DTO.CustomerDtoResponse;
import com.example.four.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class CustomerDtoMapperResponse extends DtoMapperFacade<Customer, CustomerDtoResponse> {
    public CustomerDtoMapperResponse() {
        super(Customer.class, CustomerDtoResponse.class);
    }

    @Override
    protected void decorateDto(CustomerDtoResponse dto, Customer entity) {
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setAge(entity.getAge());
        dto.setEmail(entity.getEmail());
        dto.setAccounts(entity.getAccounts());
        dto.setEmployers(entity.getEmployers());
    }
}
