package com.example.four.utilities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang3.EnumUtils;

import java.util.Optional;

/*@JsonFormat(shape = JsonFormat.Shape.OBJECT )*/
public enum Currency {
    USD("USD"),
    EUR("EUR"),
    UAH("UAH"),
    CHF("CHF"),
    GBP("GBP");

    private String val;

    Currency(String val) {
        this.val = val;
    }

    @JsonValue
    public String getValue() {
        return val;
    }

  /*  @JsonCreator // This is the factory method and must be static
    public static Currency fromString(String string) {
        return Optional
                .ofNullable(FORMAT_MAP.get(string))
                .orElseThrow(() -> new IllegalArgumentException(string));*/

    @JsonCreator
    public static Currency forValue(String name)
    {
        return EnumUtils.getEnumMap(Currency.class).get(name);
    }
}
