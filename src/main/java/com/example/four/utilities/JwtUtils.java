package com.example.four.utilities;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.example.four.model.JwtAuthentication;
import io.jsonwebtoken.Claims;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.util.*;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JwtUtils {

    public static JwtAuthentication generate(Claims claims) {
        final JwtAuthentication jwtInfoToken = new JwtAuthentication();
        jwtInfoToken.setRoles(getRoles(claims));
        jwtInfoToken.setFirstName(claims.get("firstName", String.class));
        jwtInfoToken.setUsername(claims.getSubject());
        return jwtInfoToken;
    }

    private static Set<Roles> getRoles(Claims claims) {
        final List<String> roles = claims.get("roles", List.class);
        return Arrays.stream(claims.get("roles").toString().split(","))
                .map(el -> Roles.valueOf(el.substring(el.indexOf("=")+1, el.indexOf("}"))))
                .collect(Collectors.toSet());
    }

    @RequiredArgsConstructor
    public static enum Roles implements GrantedAuthority {
        USER("USER"), ADMIN("ADMIN");
        private final String value;

        @Override
        public String getAuthority() {

            return value;
        }
    }
}

