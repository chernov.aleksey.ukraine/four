package com.example.four.Dao;

import com.example.four.model.Employer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployerJpaRepository extends JpaRepository<Employer, Long> {

}
