BEGIN;

INSERT INTO customers (name, email, age, phone, password) VALUES ('Ivan Petrenko', 'ivanpetrenko@mail.com', 22, '+38067123456789', 'ivanPetrenko');
INSERT INTO customers (name, email, age, phone, password) VALUES ('Oksana Petrenko', 'oksanapetrenko@mail.com', 21, '+38067123456789', 'oksanaPetrenko');
INSERT INTO customers (name, email, age, phone, password) VALUES ('Sergiy Stepanenko', 'sergiystepanenko@mail.com', 24, '+38067123456789', 'sergiyStepanenko');
INSERT INTO customers (name, email, age, phone, password) VALUES ('Galyna Stepanenko', 'galynastepanenko@mail.com', 23, '+38067123456789', 'galynaStepanenko');
INSERT INTO customers (name, email, age, phone, password) VALUES ('Petro Ivanenko', 'petroivanenko@mail.com', 23, '+38067123456789', 'petroIvanenko');
INSERT INTO customers (name, email, age, phone, password) VALUES ('Natalya Ivanenko', 'natalyaivanenko@mail.com', 22, '+38067123456789', 'natalyaIvanenko');
INSERT INTO customers (name, email, age, phone, password) VALUES ('Stepan Vasylenko', 'stepanvasylenko@gmail.com', 21, '+38067123456789', 'stepanVasylenko');
INSERT INTO customers (name, email, age, phone, password) VALUES ('Maryna Vasylenko', 'marynavasylenko@gmail.com', 20, '+38067123456789', 'marynaVasylenko');
INSERT INTO customers (name, email, age, phone, password) VALUES ('Oleksiy Chernenko', 'oleksiychernenko@gmail.com', 25, '+38067123456789', 'oleksiyChernenko');
INSERT INTO customers (name, email, age, phone, password) VALUES ('Kateryna Chernenko', 'katerynachernenko@gmail.com', 24, '+38067123456789', 'katerynaChernenko');
INSERT INTO customers (name, email, age, phone, password) VALUES ('Sashko Gaplychenko', 'sashkogaplychenko@gmail.com', 22, '+38067123456789', 'sashkoGaplychenko');
INSERT INTO customers (name, email, age, phone, password) VALUES ('Mariya Gaplychenko', 'mariyagaplychenko@gmail.com', 21, '+38067123456789', 'mariyaGaplychenko');
INSERT INTO customers (name, email, age, phone, password) VALUES ('Ihor Serhienko', 'ihorserhienko@gmail.com', 25, '+38067123456789', 'ihorSerhienko');


INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('82560eb9-c8f9-4be9-aaef-17fd129cec6e', 'UAH', 0, 1);

INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('f02683e2-519e-487b-b855-5286182ad1d4', 'USD', 0, 2);

INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('6295e6db-667e-4d1b-adc1-dce573b01942', 'UAH', 0,2);

INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('477b0882-84a5-41c3-87d8-a3b26cb7278f', 'EUR', 0,3);

INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('45d91b68-a4dd-4de3-88dd-13e106b12a0c', 'EUR', 0,5);

INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('4e204fcd-d296-483c-964c-74d0be4130bb', 'CHF', 0,6);

INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('9db430b11-6b5c-4d63-9663-4f59974ea04', 'USD', 0,7);

INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('6e49c4c9-c93a-48af-a37c-0af6d1db9d7d', 'UAH', 0,7);

INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('4f3d9294-a79e-48b5-8109-c934727f8edf', 'UAH', 0,9);

INSERT INTO employers
    (name, address)
    VALUES
    ('Google', 'us');

INSERT INTO employers
     (name, address)
     VALUES
     ('Microsoft', 'us');

INSERT INTO employers
     (name, address)
     VALUES
     ('Boston Dynamics', 'us');

INSERT INTO customerEmployment
    (customer_id, employer_id)
    VALUES
    (1, 1);

INSERT INTO customerEmployment
    (customer_id, employer_id)
    VALUES
    (2, 3);

 INSERT INTO customerEmployment
     (customer_id, employer_id)
     VALUES
     (3, 1);

 INSERT INTO customerEmployment
     (customer_id, employer_id)
     VALUES
     (3, 2);

 INSERT INTO customerEmployment
     (customer_id, employer_id)
     VALUES
     (4, 2);

  INSERT INTO customerEmployment
      (customer_id, employer_id)
      VALUES
      (5, 3);

  INSERT INTO customerEmployment
      (customer_id, employer_id)
      VALUES
      (7, 1);
 INSERT INTO customerEmployment
     (customer_id, employer_id)
     VALUES
     (8, 2);

 INSERT INTO customerEmployment
     (customer_id, employer_id)
     VALUES
     (9, 2);

  INSERT INTO customerEmployment
      (customer_id, employer_id)
      VALUES
      (9, 3);

  INSERT INTO customerEmployment
      (customer_id, employer_id)
      VALUES
      (10, 1);

INSERT INTO users(enabled, encrypted_password, user_name) VALUES
 (true, '$2a$10$BXH1wlAJPIMXvjnJTBoRuea4CvZwSs8/Zqz4bDRZBDJ6hxvXoHlqq', 'a'),
 (true, '$2a$10$BXH1wlAJPIMXvjnJTBoRuea4CvZwSs8/Zqz4bDRZBDJ6hxvXoHlqq', 'admin');

INSERT INTO roles(role_name, user_id) VALUES
 ('USER', 1),
 ('ADMIN', 2),
 ('ADMIN', 1);

COMMIT;
